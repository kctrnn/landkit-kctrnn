const navbarShowBtn = document.getElementById('navbarToggleShow');
const navbarCloseBtn = document.getElementById('navbarToggleClose');

const navbar = document.getElementById('navbar');
const menu = document.getElementById('navbarMenu');

const dropdownToggleList = document.querySelectorAll('.dropdown-toggle');
const dropdownMenuList = document.querySelectorAll('.dropdown-menu');

// Toggle navbar menu
navbarShowBtn.addEventListener('click', () => {
  menu.classList.add('show');

  document.body.style.overflowY = 'hidden';
  navbar.style.height = '100vh';
});

navbarCloseBtn.addEventListener('click', () => {
  menu.classList.remove('show');

  document.body.style.overflowY = 'auto';
  navbar.style.height = 'auto';
});

// Toggle dropdown
dropdownToggleList.forEach((item, index) =>
  item.addEventListener('click', () => {
    dropdownMenuList[index].classList.toggle('show');
    item.classList.toggle('active');
  })
);

// ---------- Switch billing ----------
const switchBtn = document.getElementById('switchBilling');
const switchInput = document.getElementById('billing');

const numberBilling = document.getElementById('numberBilling');

const MIN = 29;
const MAX = 49;

function updateNumber(initNum, element, finalNum) {
  let count = initNum;

  if (initNum < finalNum) {
    let timer = setInterval(() => {
      count++;

      if (count === finalNum) {
        clearInterval(timer);
      }

      element.textContent = count;
    }, 30);
  }

  if (initNum > finalNum) {
    let timer = setInterval(() => {
      count--;

      if (count === finalNum) {
        clearInterval(timer);
      }

      element.textContent = count;
    }, 30);
  }
}

switchBtn.addEventListener('click', () => {
  if (!switchInput.checked) {
    updateNumber(MIN, numberBilling, MAX);
  }

  if (switchInput.checked) {
    updateNumber(MAX, numberBilling, MIN);
  }
});

// ---------- SUBMIT FORM ----------
const formElement = document.getElementById('formAbout');

const cardNameElement = document.getElementById('cardName');
const cardEmailElement = document.getElementById('cardEmail');
const cardPasswordElement = document.getElementById('cardPassword');

const NAME_REQUIRED = 'Please enter your name';
const PASSWORD_INVALID = 'Password must be have at least 8 characters long';
const EMAIL_INVALID = 'Please enter a correct email address format';

const handleSubmit = (e) => {
  e.preventDefault();

  const nameValid = nameIsValid(cardNameElement);
  const emailValid = emailIsValid(cardEmailElement);
  const passwordValid = passwordIsValid(cardPasswordElement);

  if (nameValid && emailValid && passwordValid) {
    console.log('Name: ', cardNameElement.value);
    console.log('Email: ', cardEmailElement.value);
    console.log('Password: ', cardPasswordElement.value);

    // Reset value
    formElement.reset();
  }
};

if (formElement) {
  formElement.addEventListener('submit', handleSubmit);
}

function showMessage(input, message, isValid) {
  const msg = input.parentNode.querySelector('small');
  msg.innerText = message;

  input.className = isValid ? 'success' : 'error';
  return isValid;
}

function showError(input, message) {
  return showMessage(input, message, false);
}

function showSuccess(input) {
  return showMessage(input, '', true);
}

function nameIsValid(input) {
  if (input.value.trim() === '') return showError(input, NAME_REQUIRED);
  return showSuccess(input);
}

function emailIsValid(input) {
  const emailRegex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const email = input.value.trim();

  if (!emailRegex.test(email)) {
    return showMessage(input, EMAIL_INVALID);
  }

  return showSuccess(input);
}

function passwordIsValid(input) {
  if (input.value.trim().length < 8)
    return showMessage(input, PASSWORD_INVALID);

  return showSuccess(input);
}
