// Animation on scroll with vanilla Javascript

const scrollOffset = 100;
let isStop = false;

const scrollElements = document.querySelectorAll('.js-scroll');

// Detect when the element is within the viewport
const elementInView = (el, offset = 0) => {
  const elementTop = el.getBoundingClientRect().top;

  return (
    elementTop <=
    (window.innerHeight || document.documentElement.clientHeight) - offset
  );
};

// Assign the scrolled class name to the element if it is in view
const displayScrollElement = (element) => {
  element.classList.add('scrolled');
};

const hideScrollElement = (element) => {
  element.classList.remove('scrolled');
};

// ---------- Increase Number Animation ----------
const statCount = document.querySelector('.stat-count');

function increaseNumberRec(initNum, element, elementNum) {
  if (initNum > elementNum) return;

  element.textContent = initNum;

  if (elementNum > 60) {
    setTimeout(() => {
      increaseNumberRec(initNum + 1, element, elementNum);
    }, 10);
  } else {
    setTimeout(() => {
      increaseNumberRec(initNum + 1, element, elementNum);
    }, 50);
  }
}

const increaseNumber = () => {
  const elementList = document.querySelectorAll('.increase-num');

  if (isStop) return;

  elementList.forEach((element) => {
    const elementNum = Number(element.textContent);
    increaseNumberRec(0, element, elementNum);
  });
};

const handleScrollAnimation = () => {
  scrollElements.forEach((el) => {
    if (elementInView(el, 100)) {
      displayScrollElement(el);
    } else {
      hideScrollElement(el);
    }
  });

  if (elementInView(statCount)) {
    increaseNumber();
    isStop = true;
  } else {
    isStop = false;
  }
};

window.addEventListener('scroll', () => {
  handleScrollAnimation();
});
