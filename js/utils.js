const utils = {
  resetElementNode: (element) => {
    if (element) {
      while (element.firstChild) {
        element.removeChild(element.firstChild);
      }
    }
  },

  getMarkColor: (mark) => {
    if (mark >= 8) return 'green';
    if (mark >= 4) return 'goldenrod';

    return 'red';
  },
};

export default utils;
